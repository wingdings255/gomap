package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	log.SetPrefix("Gomap: ")
	log.SetFlags(0)

	arglen := len(os.Args[1:])
	if arglen == 0 {
		arg := "help"
		help(arg)
	} else {
		ip := string(os.Args[1])
		fmt.Printf("Scanning %s\n", ip)
	}
}

func help(cmd string) {
	switch cmd {
	case "help":
		fmt.Printf("Usage: gomap 192.168.1.1\n")
	}
}
